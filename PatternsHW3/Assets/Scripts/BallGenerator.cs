﻿//Zac Lindsey

using UnityEngine;
using System.Collections;

public class BallGenerator : MonoBehaviour {

    public Rigidbody toMake;
    public float timeInterval = 4.0f;
	// Use this for initialization
	void Start () {
        InvokeRepeating("makeBalls", timeInterval, timeInterval);
    }
	
	// Update is called once per frame
	void makeBalls()
    {
        Instantiate(toMake, transform.position, transform.rotation);
    }
}
