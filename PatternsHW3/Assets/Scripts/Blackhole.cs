﻿using UnityEngine;
using System.Collections;

public class Blackhole : MonoBehaviour {

	public GameObject endPoint;


	void OnTriggerEnter(Collider other)
	{
		GameObject ball = other.gameObject ;
		other.transform.position = endPoint.transform.position; 
		Rigidbody rb = ball.GetComponent < Rigidbody> ();
		rb.AddForce (0, 0, -2); 
	}
}
