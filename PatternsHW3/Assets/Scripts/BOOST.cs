﻿using UnityEngine;
using System.Collections;

public class BOOST : MonoBehaviour
{
	public float boost;

	void OnTriggerEnter (Collider other)
    {
        if (other.tag == "BALL_TAG")
            other.GetComponent<Rigidbody>().AddForce (0, 0, boost, ForceMode.Impulse);
	}
}
