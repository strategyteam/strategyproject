﻿// Kai Gustafson

using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public float rotateSpeed;
    public float positionSpeed;

	void Update ()
    {
	    if(Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * positionSpeed, Camera.main.transform);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * positionSpeed, Camera.main.transform);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * positionSpeed, Camera.main.transform);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * positionSpeed, Camera.main.transform);
        }

        if (Input.GetMouseButton(1))
        {
            float h = rotateSpeed * Input.GetAxis("Mouse X");
            float v = rotateSpeed * Input.GetAxis("Mouse Y");
            transform.Rotate(0, h, v, Space.World);
        }
	}
}
