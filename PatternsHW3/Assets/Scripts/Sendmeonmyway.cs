﻿using UnityEngine;
using System.Collections;

public class Sendmeonmyway : MonoBehaviour
{
    public float spd;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BALL_TAG")
        {
            other.GetComponent<Rigidbody>().AddForce(transform.forward * spd);
        }
    }
}
