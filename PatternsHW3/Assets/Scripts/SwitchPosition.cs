﻿using UnityEngine;
using System.Collections;

public class SwitchPosition : MonoBehaviour {

    public GameObject trackPiece;
    public GameObject newPosition;
    private GameObject original;
    private bool clicked;

	void OnMouseDown()
    {
        if (!clicked)
        {
            clicked = true;
            original = new GameObject();
            original.transform.position = new Vector3(trackPiece.transform.position.x,
                                            trackPiece.transform.position.y,
                                            trackPiece.transform.position.z);
            original.transform.rotation = trackPiece.transform.rotation;
        }
        if (trackPiece.transform.position != original.transform.position)
        {
            trackPiece.transform.position = original.transform.position;
            trackPiece.transform.rotation = original.transform.rotation;
        }
        else
        {
            trackPiece.transform.position = newPosition.transform.position;
            trackPiece.transform.rotation = newPosition.transform.rotation;
        }
    }
}
