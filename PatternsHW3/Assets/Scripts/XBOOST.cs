﻿using UnityEngine;
using System.Collections;

public class XBOOST : MonoBehaviour {

	public Rigidbody rb;
	public float boost;
	// Use this for initialization

	// Update is called once per frame
	void OnTriggerEnter (Collider other) {

		rb.AddForce (boost, 0, 0, ForceMode.Impulse);
	}
}