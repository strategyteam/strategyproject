﻿//Modified by Zac Lindsey

using UnityEngine;
using System.Collections;

public class zBOOST : MonoBehaviour {
	public float boost;

	void OnTriggerEnter (Collider other) {
        if (other.tag=="BALL_TAG")
            other.GetComponent<Rigidbody>().AddForce (transform.forward*boost);
	}

}
