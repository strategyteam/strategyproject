﻿//Zac Lindsey
//Shrink 

using UnityEngine;
using System.Collections;

public class zExpandMechanism : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BALL_TAG")
        {
            if(other.transform.localScale == new Vector3(0.5f, 0.5f, 0.5f))
                other.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f);
        }

    }
}
