﻿using UnityEngine;
using System.Collections;

public class Movemahball : MonoBehaviour
{
    public float spd;

    void onTriggerEnter(Collider other)
    {
        if (other.tag == "BALL_TAG")
        {
            other.GetComponent<Rigidbody>().AddForce(transform.forward / spd);
        }
    }
}
